vary_random(x, mininum_percentage, maximum_percentage) = x * (1 + rand(mininum_percentage:maximum_percentage) / 100)

function random_points_near(point::Tuple, number_of_points::Int = 10,
        minimum_variation_percentage::Int = -10, maximum_variation_percetage::Int = 10)
    
    x, y = point
    
    return [
                (
                    vary_random(x, minimum_variation_percentage, maximum_variation_percetage),
                    vary_random(y, minimum_variation_percentage, maximum_variation_percetage)
                ) for i = 1:number_of_points
           ]
end
