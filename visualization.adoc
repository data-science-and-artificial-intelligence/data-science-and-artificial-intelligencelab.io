= Visualization

Humans are visual animals, nearly asciimath:[frac{2}{3}]rd of human brain is used for vision they say. So if you get right view of the data, we can say a lot about it. We will be using Julia as programming language in this book, since this is a practical hands on book, let's see how to get started with Visualization.


== Julia Plots

There is a package called Plotsfootnote:[https://docs.juliaplots.org/latest/] in Julia that will help you with creating graphs and visualizations. Let's see about it.

To get started you first need to install it, fire up your notebook, and in it type this in the first cell

=== Types of plots

[source, julia]
----
using Pkg
Pkg.add("Plots")
using Plots
----

and press kbd:[SHIFT + ENTER]. If you are lucky before A.D 3000, the necessary stuff would have been installed and your plotting library would be ready for use.

image::images/visualization-39f69.png[]

Next let's generate an empty plot. Type the following in the next call

[source, julia]
----
plot()
----

and press kbd:[SHIFT + ENTER], you will be seeing an empty plot as shown below.

image::images/visualization-2debb.png[]

Now let's start by plotting some values, type the code below and execute it

[source, julia]
----
y = [1, 3, 6, 5, 7]
plot(y)
----

as you can see below the values are plotted in y-axis, if nothing is given, x-axis is assumed to be 1, 2, 3, 4 and so on.

image::images/visualization-fc870.png[]

Now a graph is usually a plot on a x-y plane, so lets plot with some x and y values, type in the code below and execute the cell

[source, julia]
----
x = [0, 1, 2, 3, 4]
y = [1, 3, 6, 5, 7]
plot(x, y)
----

as you can see, you will get kinda same plot as above, but in the x-axis it starts from 0 since we have given `x = [0, 1, 2, 3, 4]`

image::images/visualization-577e3.png[]

Now let's see how to plot a bar chart, rather than use `plot()` tht usually plots a line, we use the `bar()` function as shown

[source, julia]
----
x = [0, 1, 2, 3, 4]
y = [1, 3, 6, 5, 7]
bar(x, y)
----

The above code generates a beautiful bar chart as shown below.

image::images/visualization-8e010.png[]

Like bar we can have a pie chart too, as shown

[source, julia]
----
x = ["Ten", "Twenty", "Thirty", "Forty"]
y = [10, 20, 30, 40]
pie(x, y)
----

image::images/visualization-c4dcb.png[]

Scatter plot is one the thing which I use a lot for data science and it's accomplished by the function `scatter()` as shown below

[source, julia]
----
x = [0, 1, 2, 3, 4, 1, 3, 6, 5, 7]
y = [1, 3, 6, 5, 7, 0, 1, 2, 3, 4]
scatter(x, y)
----

image::images/visualization-e2370.png[]

Next are the histograms, if you want to see if values are concentrated around something you can use it. In the code below we call a `histogam()` function to which we pass an array of numbers and this function is intelligent enough to plot it out

[source, julia]
----
histogram([0, 1, 1, 5, 5, 5, 5, 7, 9, 0, 7, 3, 4, 11], bins = 5)
----

image::images/visualization-39e24.png[]

If you want to see where the numbers are clustered around, then you can use these plots.

=== Documenting Plots

[source julia]
----
using Plots

x = (0:0.001:2π)
sinx = [sin(val) for val in x]
cosx = [cos(val) for val in x]

plot(
    x, [sinx, cosx],
    title = "Trignometric Functions",
    xlabel = "Angle in Radians",
    ylabel = "Amplitude",
    labels = ["sin(x)" "cos(x)"]
)
----

image::images/visualization-e5308.png[]

== Great Data Visualizations

=== Napoleons Russian Invasion

image::https://upload.wikimedia.org/wikipedia/commons/2/29/Minard.png[]

https://en.wikipedia.org/wiki/French_invasion_of_Russia

=== 1854 Broad Street cholera outbreak

image::https://upload.wikimedia.org/wikipedia/commons/2/27/Snow-cholera-map-1.jpg[]

https://en.wikipedia.org/wiki/1854_Broad_Street_cholera_outbreak

=== Hans Rosling's Visualization

video::jbkSRLYSojo[youtube]

